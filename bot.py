from bot.training 	import *
from bot.chatbot 	import chatbot


if __name__ == "__main__":

	print("ALICE CHATBOT.\n- Type \q to exit.\n- Type \\train to train this bot.")

	while(True):
		try:
			userInput = input("You:> ")

			if userInput == "\q":
				break
			elif userInput == "\\train":
				chatbot = train_bot()
			else:
				response = chatbot.get_response(str(userInput))
				print("ALICE:> %s"%response)
		except(KeyboardInterrupt, EOFError, SystemExit):
			break