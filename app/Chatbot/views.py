from app 			import app
from flask 			import Flask, render_template, request, session, jsonify

from bot.training 	import *
from bot.chatbot	import chatbot

@app.route("/chatbot", methods=['GET','POST'])
def chat():
	if(request.method == "POST"):
		try:
			Statement = request.form['messageText']
			Response  = chatbot.get_response(str(Statement))
			return jsonify(answer = str(Response))
		except Exception as e:
			return jsonify(answer = "Something went wrong %s"%e)


	return render_template('chatbot/chatbot.html')

