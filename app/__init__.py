# -*- coding: utf-8 -*-
from flask 				import Flask, render_template, request, session
from flask_sqlalchemy 	import SQLAlchemy


# create flask instance
app = Flask("app")
app.config.update(	SQLALCHEMY_DATABASE_URI = 'postgres://postgres:postgres@localhost:5432/dev',
					SQLALCHEMY_TRACK_MODIFICATIONS = True)

db = SQLAlchemy(app)

# register blueprint
# from admin import admin # the Blueprint instance that we created
# app.register_blueprint(admin, url_prefix='/admin')


from app.Chatbot.views 	import *


@app.route("/")
def index():
	return "Chatterbot"
