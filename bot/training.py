# -*- coding: utf-8 -*-
from bot.chatbot 			import chatbot
from chatterbot.trainers 	import UbuntuCorpusTrainer


def train_bot():
	# using corpus training
	chatbot.set_trainer(UbuntuCorpusTrainer)
	chatbot.train()

	return chatbot