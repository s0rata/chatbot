from chatterbot 		import ChatBot

# create a chat bot instance
chatbot = ChatBot(	"ALICE",
					logic_adapters=[
				        'chatterbot.logic.BestMatch'
				    ],
				    storage_adapter="chatterbot.storage.SQLStorageAdapter",
				    database_uri="postgres://postgres:postgres@localhost:5432/dev",
				    trainer='chatterbot.trainers.UbuntuCorpusTrainer'
				)
